import { createConnection } from 'typeorm';

export const databaseProviders = [
    {
        provide: 'DATABASE_CONNECTION',
        useFactory: async () => await createConnection({
            type: 'mariadb',
            host: process.env.DATABASE_HOST,
            port: parseInt(process.env.DATABASE_PORT, 10),
            username: process.env.DATABASE_USERNAME,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            entities: [
                __dirname + '/../**/*.entity{.ts,.js}',
            ],
            synchronize: process.env.ENVIRONMENT === "DEVELOPMENT" ? true : false,
            // debug: true,
        }),
    },
];