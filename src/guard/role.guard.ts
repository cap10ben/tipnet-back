import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { ROLES } from 'src/core/profile/entities/role.enum';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(
    private _reflector: Reflector,
    private _jwtService: JwtService
  ) { }

  canActivate(ctx: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {

    const requiredRoles = this._reflector.get<number[]>('roles', ctx.getHandler());
    if (!requiredRoles || requiredRoles.includes(ROLES.GHOST)) return true;

    const token = ctx.switchToHttp().getRequest().headers.authorization?.split(' ')[1] as string;
    if(!token) return false;
    const userRole = this._jwtService.decode(token)['role'];
    if (requiredRoles.includes(userRole)) return true;

    return false;
  }
}
