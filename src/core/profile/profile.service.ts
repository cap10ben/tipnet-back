import { ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BankrollService } from '../bankroll/bankroll.service';
import { Bankroll } from '../bankroll/entities/bankroll.entity';
import { User } from '../user/entities/user.entity';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { IProfile } from './entities/IProfile';
import { Profile } from './entities/profile.entity';
import { ROLES } from './entities/role.enum';
import { mapToFullDto, mapToLightDto } from './profile.mapper';

@Injectable()
export class ProfileService {

  /**
   *
   */
  constructor(
    @Inject('PROFILE_REPOSITORY') private repository: Repository<Profile>,
    @Inject('USER_REPOSITORY') private userRepository: Repository<User>,
    @Inject('BANKROLL_REPOSITORY') private bankrollRepository: Repository<Bankroll>,
  ) { }

  //#region CRUD
  async create(createProfileDto: CreateProfileDto, user: User) {
    if (createProfileDto.role === ROLES.ADMIN || createProfileDto.role === ROLES.GHOST) {
      await this.userRepository.delete({id: user.id});
      throw new ForbiddenException();
    }

    const toCreate: IProfile = {
      lastname: createProfileDto.lastname,
      firstname: createProfileDto.firstname,
      birthdate: new Date(createProfileDto.birthdate),
      phone: createProfileDto.phone,
      role: createProfileDto.role,
      following: [],
      follower: [],
      advicer: [],
      adviced: [],
      tip: [],
      tipFollowing: [],
      user,
    }

    const profileCreated = await this.repository.save(toCreate);
    const bankroll = {
      title: 'default',
      profile: profileCreated,
      isActive: true
    }
    
    await this.bankrollRepository.save(bankroll);
  }

  async findAll(): Promise<Partial<IProfile>[]> {
    const users = await this.repository.find();
    return users.map(mapToLightDto);
  }

  //TODO make 1 end-point by relations
  async findOne(id: number, profileId?: number, role?: ROLES): Promise<IProfile> {
    const user = await this.repository.findOne(id, {
      relations: [
        'follower',
        'following',
        'advicer',
        'adviced',
        'tip',
        'bankroll',
        'user',
        'tipFollowing'
      ]
    });
    if (!user) throw new NotFoundException();
    // if resource is requested by his owner or by ADMIN
    if (id === profileId || role === ROLES.ADMIN) {
      return mapToFullDto(user);
    }
    delete user.user;
    user.tip = user.tip.filter(t => !t.isPrivate);
    user.bankroll = user.bankroll.filter(b => b.isActive);
    return mapToFullDto(user);
  }

  async update(id: number, updateProfileDto: UpdateProfileDto): Promise<IProfile> {
    const toUpdate: Partial<IProfile> = {
      lastname: updateProfileDto.lastname,
      firstname: updateProfileDto.firstname,
      birthdate: new Date(updateProfileDto.birthdate),
      phone: updateProfileDto.phone,
    }
    await this.repository.update({ id }, toUpdate);
    // TODO handle update errors
    return this.findOne(id);
  }

  async remove(id: number, forceDelete: boolean = false) {
    // TODO check resource owner or role admin
    const toSoftDelete = await this.repository.findOne(id);
    if (forceDelete) {
      return this.repository.remove([toSoftDelete]);
    } else {
      return this.repository.softRemove(toSoftDelete);
    }
  }
  //#endregion CRUD

  async follow(profileId: number, id: number) {
    //* can't follow itslef
    if(profileId === id ) throw new ForbiddenException()
    const authUser = await this.repository.findOne(profileId,  { relations: ['following'] });
    const toFollow = await this.repository.findOne(id);
    if (!toFollow) throw new NotFoundException();
    if (authUser.following) {
      authUser.following.push(toFollow);
    } else {
      authUser.following = [toFollow];
    }
    return await this.repository.save(authUser);
  }

  async unfollow(profileId: number, id: number) {
    //* can't unfollow itslef
    if(profileId === id ) throw new ForbiddenException()
    const authUser = await this.repository.findOne(profileId, { relations: ['following'] });
    const toUnfollow = await this.repository.findOne(id);
    if (!toUnfollow) throw new NotFoundException();
    if (authUser.following) {
      const i = authUser.following.findIndex(p => p.id === toUnfollow.id);
      if (i > -1) {
        authUser.following.splice(i, 1);
      } else {
        throw new NotFoundException()
      }
    } else {
      authUser.following = [];
    }
    return this.repository.save(authUser);
  }

  async advice(profileId: number, id: number) {
    //* can't advice itslef
    if(profileId === id ) throw new ForbiddenException()
    const authUser = await this.repository.findOne(profileId, { relations: ['adviced'] });
    const toAdvice = await this.repository.findOne(id);
    if (!toAdvice) throw new NotFoundException();
    if (authUser.adviced) {
      authUser.adviced.push(toAdvice);
    } else {
      authUser.adviced = [toAdvice];
    }
    return await this.repository.save(authUser);
  }

  async unadvice(profileId: number, id: number) {
    //* can't unadvice itslef
    if(profileId === id ) throw new ForbiddenException()
    const authUser = await this.repository.findOne(profileId, { relations: ['adviced'] });
    const toUnadvice = await this.repository.findOne(id);
    if (!toUnadvice) throw new NotFoundException();
    if (authUser.adviced) {
      const i = authUser.adviced.findIndex(p => p.id === toUnadvice.id);
      if (i > -1) {
        authUser.adviced.splice(i, 1);
      } else {
        throw new NotFoundException()
      }
    } else {
      authUser.adviced = [];
    }
    return this.repository.save(authUser);
  }
}
