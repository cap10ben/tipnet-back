export enum ROLES {
    ADMIN,
    TIPSTER,
    FOLLOWER,
    GHOST
}