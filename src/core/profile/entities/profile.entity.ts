import { Bankroll } from "src/core/bankroll/entities/bankroll.entity";
import { Comment } from "src/core/comment/entities/comment.entity";
import { Conversation } from "src/core/conversation/entities/conversation.entity";
import { DiffusionList } from "src/core/diffusion-list/entities/diffusion-list.entity";
import { Like } from "src/core/like/entities/like.entity";
import { Message } from "src/core/message/entities/message.entity";
import { Post } from "src/core/post/entities/post.entity";
import { Tip } from "src/core/tip/entities/tip.entity";
import { User } from "src/core/user/entities/user.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ROLES } from "./role.enum";

@Entity('profile')
export class Profile {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstname: string;

    @Column()
    lastname: string;

    @Column()
    birthdate: Date;

    @Column({
        nullable: true
    })
    phone: string;

    @Column({
        type: 'enum',
        enum: ROLES,
        default: ROLES.FOLLOWER
    })
    role: ROLES;

    @Column({
        default: 0,
    })
    rankingPoint: number

    @OneToOne(() => User, {
        createForeignKeyConstraints: true,
        nullable: false,
    })
    @JoinColumn({
        name: 'user_id'
    })
    user: User;

    @ManyToMany(() => Profile, p => p.following)
    @JoinTable({
        name: 'profile_follower'
    })
    follower: Profile[];

    @ManyToMany(() => Profile, p => p.follower)
    following: Profile[];

    @ManyToMany(() => Profile, p => p.adviced)
    @JoinTable({
        name: 'profile_advice'
    })
    advicer: Profile[];

    @ManyToMany(() => Profile, p => p.advicer)
    adviced: Profile[];

    @OneToMany(() => Tip, t => t.profile)
    tip: Tip[];

    @OneToMany(() => Bankroll, b => b.profile)
    bankroll: Bankroll[];

    @ManyToMany(() => Tip, t => t.follower)
    @JoinTable({
        name: 'profile_tip_following'
    })
    tipFollowing: Tip[];

    @OneToMany(() => Message, m => m.author)
    messages: Message[];

    @ManyToMany(() => Conversation, c => c.participant)
    @JoinTable({
        name: 'profile_conversation'
    })
    conversation: Conversation[];

    @OneToMany(() => Conversation, c => c.createdBy)
    conversationCreated: Conversation[];

    @ManyToMany(() => DiffusionList, d => d.participant)
    @JoinTable({
        name: 'profile_diffusionList'
    })
    diffusionList: DiffusionList[];

    @OneToMany(() => DiffusionList, d => d.createdBy)
    diffusionListCreated: DiffusionList[];

    @OneToMany(() => Post, p => p.author)
    post: Post[];

    @OneToMany(() => Comment, c => c.author)
    comment: Comment[];

    @OneToMany(() => Like, l => l.author)
    like: Like[];
}
