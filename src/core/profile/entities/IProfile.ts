import { Bankroll } from "src/core/bankroll/entities/bankroll.entity";
import { ITip } from "src/core/tip/entities/ITip";
import { User } from "src/core/user/entities/user.entity";
import { ROLES } from "./role.enum";

export interface IProfile {

    id?: number;
    firstname: string;
    lastname: string;
    phone?: string;
    birthdate: Date;
    bankroll?: Partial<Bankroll>[];
    activeBankroll?: Partial<Bankroll>;
    follower: Partial<IProfile>[];
    following: Partial<IProfile>[];
    advicer: Partial<IProfile>[];
    adviced: Partial<IProfile>[];
    role?: ROLES;
    user?: User;
    tip: ITip[];
    publicTips?: ITip[];
    privateTips?: ITip[];
    tipFollowing: ITip[];
}