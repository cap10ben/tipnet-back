import { Controller, Get, Body, Patch, Param, Delete, UseGuards, UseInterceptors, Req, Post } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { JwtAuthGuard } from 'src/guard/jwt.guard';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from './entities/role.enum';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { ApiTags } from '@nestjs/swagger';

@UseGuards(JwtAuthGuard)
@Controller('profile')
@ApiTags('Profile')
export class ProfileController {
  constructor(
    private readonly profileService: ProfileService,
    ) { }

  //#region CRUD
  // @Roles(ROLES.ADMIN)
  // @UseInterceptors(AuthUserInterceptor)
  // @Post()
  // create(@Body() createProfileDto: CreateProfileDto) {
  // }

  @Roles(ROLES.GHOST)
  @Get()
  findAll() {
    return this.profileService.findAll();
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Get(':id')
  findOne(@Param('id') id: string, @Req() req) {
    const {profileId, role} = req.authInfo
    return this.profileService.findOne(+id, profileId, role);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProfileDto: UpdateProfileDto) {
    return this.profileService.update(+id, updateProfileDto);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Delete(':id')
  softRemove(@Param('id') id: string) {
    return this.profileService.remove(+id);
  }

  @Roles(ROLES.ADMIN)
  @UseInterceptors(AuthUserInterceptor)
  @Delete(':id')
  remove(@Param('id') id: string, @Param('forceDelte') forceDelte: boolean) {
    return this.profileService.remove(+id, forceDelte);
  }
  //#endregion CRUD

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id/follow')
  follow(@Param('id') id: string, @Req() req: any) {
   const authUserId: number = req.authInfo.profileId
    return this.profileService.follow(authUserId, +id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id/unfollow')
  unfollow(@Param('id') id: string, @Req() req: any) {
   const authUserId: number = req.authInfo.profileId
    return this.profileService.unfollow(authUserId, +id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id/advice')
  advice(@Param('id') id: string, @Req() req: any) {
   const authUserId: number = req.authInfo.profileId
    return this.profileService.advice(authUserId, +id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id/unadvice')
  unadvice(@Param('id') id: string, @Req() req: any) {
   const authUserId: number = req.authInfo.profileId
    return this.profileService.unadvice(authUserId, +id);
  }
}

