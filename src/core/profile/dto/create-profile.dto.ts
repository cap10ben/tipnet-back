import { ApiProperty } from "@nestjs/swagger";
import { IsDateString, IsEnum, IsOptional, IsPhoneNumber, IsString, MinLength } from "class-validator";
import { ROLES } from "../entities/role.enum";

export class CreateProfileDto {

    @ApiProperty()
    @IsString()
    @MinLength(2)
    firstname: string;

    @ApiProperty()
    @IsString()
    @MinLength(2)
    lastname: string;

    @ApiProperty()
    @IsDateString()
    birthdate: string;

    @ApiProperty()
    @IsPhoneNumber()
    @IsOptional()
    phone: string;

    @ApiProperty({
        enum: ROLES,
        required: false
    })
    @IsOptional()
    @IsEnum(ROLES)
    role: ROLES
}
