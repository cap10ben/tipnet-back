import { Module } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { ProfileController } from './profile.controller';
import { profileProviders } from './profile.providers';
import { DatabaseModule } from 'src/database/database.module';
import { JwtModule } from '@nestjs/jwt';
import { userProviders } from '../user/user.providers';
import { BankrollService } from '../bankroll/bankroll.service';
import { bankrollProviders } from '../bankroll/bankroll.providers';
import { BankrollModule } from '../bankroll/bankroll.module';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [ProfileController],
  providers: [
    ...profileProviders,
    ...userProviders,
    ...bankrollProviders,
    ProfileService,
  ],
  exports: [
    ProfileService
  ]
})
export class ProfileModule { }
