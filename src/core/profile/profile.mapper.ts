import { IProfile } from "./entities/IProfile";
import { Profile } from "./entities/profile.entity";

export function mapToFullDto (profile: Profile): IProfile {
    if(profile.user) {
        delete profile.user.password;
        delete profile.user.salt;
    }
    return {
        id: profile.id,
        birthdate: profile.birthdate,
        bankroll: profile.bankroll,
        activeBankroll: profile.bankroll.find(b => b.isActive),
        firstname: profile.firstname,
        follower: profile.follower.map(mapToLightDto),
        following: profile.following.map(mapToLightDto),
        advicer: profile.advicer.map(mapToLightDto),
        adviced: profile.adviced.map(mapToLightDto),
        lastname: profile.lastname,
        phone: profile.phone,
        tip: profile.tip,
        tipFollowing: profile.tipFollowing,
        user: profile.user,
    };
}

export function mapToLightDto (profile: Profile): Partial<IProfile> {
    return {
        id: profile.id,
        firstname: profile.firstname,
        lastname: profile.lastname,
    };
}