import { Controller } from '@nestjs/common';
import { LikeService } from './like.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Like')
@Controller('like')
export class LikeController {
  constructor(private readonly likeService: LikeService) {}

  // @Post()
  // create(@Body() createLikeDto: CreateLikeDto) {
  //   return this.likeService.create(createLikeDto);
  // }

  // @Get()
  // findAll() {
  //   return this.likeService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.likeService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateLikeDto: UpdateLikeDto) {
  //   return this.likeService.update(+id, updateLikeDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.likeService.remove(+id);
  // }
}
