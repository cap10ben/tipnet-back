import { Module } from '@nestjs/common';
import { LikeService } from './like.service';
import { LikeController } from './like.controller';
import { likeProviders } from './like.providers'
import { DatabaseModule } from 'src/database/database.module';
import { JwtModule } from '@nestjs/jwt';
import { postProviders } from '../post/post.providers';
import { profileProviders } from '../profile/profile.providers';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [LikeController],
  providers: [
    ...likeProviders,
    ...postProviders,
    ...profileProviders,
    LikeService
  ],
  exports: [LikeService]
})
export class LikeModule { }
