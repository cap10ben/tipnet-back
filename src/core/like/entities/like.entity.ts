import { Post } from "src/core/post/entities/post.entity";
import { Profile } from "src/core/profile/entities/profile.entity";
import { Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('_like')
export class Like {
    
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Profile, p => p.id, { nullable: false })
    author: Profile;

    @ManyToOne(() => Post, p => p.id, { nullable: false })
    post: Post;
}
