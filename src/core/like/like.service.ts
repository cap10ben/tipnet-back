import { ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Post } from '../post/entities/post.entity';
import { Profile } from '../profile/entities/profile.entity';
import { Like } from './entities/like.entity';

@Injectable()
export class LikeService {

  constructor(
    @Inject('LIKE_REPOSITORY') private repository: Repository<Like>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>,
    @Inject('POST_REPOSITORY') private postRepository: Repository<Post>
  ) { }

  async like(authUserId: number, postId: number) {
    const author = await this.profileRepository.findOne(authUserId);
    if(!author) throw new ForbiddenException();
    const post = await this.postRepository.findOne(postId);
    if(!post) throw new NotFoundException();
    const exist = await this.repository.find({post, author});
    if(exist.length > 0) throw new ForbiddenException();
    const toCreate = {
      author,
      post
    }
    return this.repository.save(toCreate);
  }

  async unlike(authUserId: number, postId: number) {
    const author = await this.profileRepository.findOne(authUserId);
    if(!author) throw new ForbiddenException();
    const post = await this.postRepository.findOne(postId);
    if(!post) throw new NotFoundException();
    const toRemove = await this.repository.findOne({post, author});
    if(!toRemove) throw new ForbiddenException;
    return this.repository.remove(toRemove);
  }
  //#region CRUD

  findAll() {
    return `This action returns all like`;
  }

  findOne(id: number) {
    return `This action returns a #${id} like`;
  }


  remove(id: number) {
    return `This action removes a #${id} like`;
  }
  //#endregion CRUD
}
