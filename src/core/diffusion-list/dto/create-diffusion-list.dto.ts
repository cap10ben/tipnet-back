import { ApiProperty } from "@nestjs/swagger";
import { IsArray, ArrayMinSize, IsNumber } from "class-validator";

export class CreateDiffusionListDto {
    @ApiProperty()
    @IsArray()
    @ArrayMinSize(1)
    @IsNumber({},{each: true})
    participant: number[];
}
