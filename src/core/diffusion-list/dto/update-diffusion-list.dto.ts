import { PartialType } from '@nestjs/swagger';
import { CreateDiffusionListDto } from './create-diffusion-list.dto';

export class UpdateDiffusionListDto extends CreateDiffusionListDto {}
