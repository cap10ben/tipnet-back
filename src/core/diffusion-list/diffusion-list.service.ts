import { BadRequestException, ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Profile } from '../profile/entities/profile.entity';
import { CreateDiffusionListDto } from './dto/create-diffusion-list.dto';
import { UpdateDiffusionListDto } from './dto/update-diffusion-list.dto';
import { UpdateParticipantDTO } from './dto/update-participants.dto';
import { DiffusionList } from './entities/diffusion-list.entity';

@Injectable()
export class DiffusionListService {

  constructor(
    @Inject('DIFFUSION_LIST_REPOSITORY') private repository: Repository<DiffusionList>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>
  ) { }
  async create(profileId: number, createDiffusionListDto: CreateDiffusionListDto) {
    if (createDiffusionListDto.participant.includes(profileId)) throw new BadRequestException();

    const creator = await this.profileRepository.findOne(profileId);
    if (!creator) throw new NotFoundException();

    const participants = await this.profileRepository.findByIds(createDiffusionListDto.participant);
    if (participants.length === 0) throw new NotFoundException();

    const toCreate = {
      createdBy: creator,
      participant: participants,
    }
    return this.repository.save(toCreate);
  }

  async findAll(profileId: number) {
    const authUser = await this.profileRepository.findOne(profileId, { relations: ['diffusionList'] });
    if (!authUser) throw new NotFoundException();
    const difListIds = authUser.diffusionList.map(c => c.id);
    return this.repository.findByIds(difListIds);
  }

  async findOne(profileId: number, id: number) {
    const authUser = await this.profileRepository.findOne(profileId);
    if (!authUser) throw new NotFoundException();
    const difList = await this.repository.findOne(id, { relations: ['participant', 'message'] });
    if (!difList) throw new NotFoundException();
    const isParticipant = difList.participant.some(p => p.id === authUser.id);
    if (!isParticipant) throw new ForbiddenException();
    return difList;
  }

  // update(id: number, updateConversationDto: UpdateConversationDto) {
  //   return `This action updates a #${id} diffusion list`;
  // }

  remove(id: number) {
    return `This action removes a #${id} diffusion list`;
  }
  //#endregion CRUD

  async addParticipant(profileId: number, id: number, updateParticipantDTO: UpdateParticipantDTO) {
    const difList = await this.repository.findOne(id, { relations: ['participant', 'createdBy'] });
    if (!difList) throw new NotFoundException();
    if (difList.createdBy.id !== profileId) throw new ForbiddenException();
    const participants = await this.profileRepository.findByIds(updateParticipantDTO.participant);
    difList.participant.push(...participants);
    await this.repository.save(difList);
    return await this.repository.findOne(id, { relations: ['participant', 'createdBy'] });
  }

  //TODO handle delete difList when participant number < 2
  async removeParticipant(profileId: number, id: number, updateParticipantDTO: UpdateParticipantDTO) {
    const difList = await this.repository.findOne(id, { relations: ['participant', 'createdBy'] });
    if (!difList) throw new NotFoundException();
    if (difList.createdBy.id !== profileId) throw new ForbiddenException();
    if (updateParticipantDTO.participant.includes(difList.createdBy.id)) throw new BadRequestException();
    const toRemove = await this.profileRepository.findByIds(updateParticipantDTO.participant);
    toRemove.forEach(r => {
      const i = difList.participant.findIndex(p => p.id === r.id);
      if (i > -1) difList.participant.splice(i, 1);
    });
    return this.repository.save(difList);
  }
}
