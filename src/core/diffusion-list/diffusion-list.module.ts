import { Module } from '@nestjs/common';
import { DiffusionListService } from './diffusion-list.service';
import { DiffusionListController } from './diffusion-list.controller';
import { DatabaseModule } from 'src/database/database.module';
import { diffusionListProviders } from './diffusion-list.providers';
import { JwtModule } from '@nestjs/jwt';
import { profileProviders } from '../profile/profile.providers';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [
    DiffusionListController
  ],
  providers: [
    ...diffusionListProviders,
    ...profileProviders,
    DiffusionListService
  ]
})
export class DiffusionListModule {}
