import { Connection } from "typeorm";
import { DiffusionList } from "../diffusion-list/entities/diffusion-list.entity";

export const diffusionListProviders = [
    {
        provide: 'DIFFUSION_LIST_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(DiffusionList),
        inject: ['DATABASE_CONNECTION']
    }
]