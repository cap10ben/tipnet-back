import { Test, TestingModule } from '@nestjs/testing';
import { DiffusionListController } from './diffusion-list.controller';
import { DiffusionListService } from './diffusion-list.service';

describe('DiffusionListController', () => {
  let controller: DiffusionListController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DiffusionListController],
      providers: [DiffusionListService],
    }).compile();

    controller = module.get<DiffusionListController>(DiffusionListController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
