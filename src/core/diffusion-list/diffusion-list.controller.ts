import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, Req, Res } from '@nestjs/common';
import { ApiNotImplementedResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from '../profile/entities/role.enum';
import { DiffusionListService } from './diffusion-list.service';
import { CreateDiffusionListDto } from './dto/create-diffusion-list.dto';
import { UpdateDiffusionListDto } from './dto/update-diffusion-list.dto';
import { UpdateParticipantDTO } from './dto/update-participants.dto';

@ApiTags('Diffusion List')
@Controller('diffusion-list')
@UseInterceptors(AuthUserInterceptor)
export class DiffusionListController {
  constructor(private readonly diffusionListService: DiffusionListService) {}

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Post()
  create(@Body() createDiffusionListDto: CreateDiffusionListDto, @Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.diffusionListService.create(authUserId, createDiffusionListDto);
  }

  @Get()
  findAll(@Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.diffusionListService.findAll(authUserId);
  }

  @Get(':id')
  findOne(@Param('id') id: string, @Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.diffusionListService.findOne(authUserId, +id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDiffusionListDto: UpdateDiffusionListDto, @Req() req, @Res() res: Response) {
    return res.sendStatus(501);
    // const authUserId: number = req.authInfo.profileId;
    // return this.diffusionListService.update(authUserId, +id, updateDiffusionListDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Req() req, @Res() res: Response) {
    return res.sendStatus(501);
    // const authUserId: number = req.authInfo.profileId;
    // return this.diffusionListService.remove(+id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Patch(':id/participant/add')
  addParticipants(@Param('id') id: string, @Body() updateParticipantDTO: UpdateParticipantDTO, @Req() req) {
   const authUserId: number = req.authInfo.profileId;
    return this.diffusionListService.addParticipant(authUserId, +id, updateParticipantDTO);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Patch(':id/participant/remove')
  removeParticipant(@Param('id') id: string, @Body() updateParticipantDTO: UpdateParticipantDTO, @Req() req) {
   const authUserId: number = req.authInfo.profileId;
    return this.diffusionListService.removeParticipant(authUserId, +id, updateParticipantDTO);
  }
}
