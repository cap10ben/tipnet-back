import { Test, TestingModule } from '@nestjs/testing';
import { DiffusionListService } from './diffusion-list.service';

describe('DiffusionListService', () => {
  let service: DiffusionListService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DiffusionListService],
    }).compile();

    service = module.get<DiffusionListService>(DiffusionListService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
