import { Message } from "src/core/message/entities/message.entity";
import { Profile } from "src/core/profile/entities/profile.entity";
import { CreateDateColumn, DeleteDateColumn, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('diffusion_list')
export class DiffusionList {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Profile, p => p.id, { nullable: false })
    createdBy: Profile;

    @ManyToMany(() => Profile, p => p.diffusionList)
    participant: Profile[];

    @OneToMany(() => Message, m => m.diffusionList)
    message: Message[];

    @CreateDateColumn()
    createdAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
