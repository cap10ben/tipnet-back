import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { DatabaseModule } from 'src/database/database.module';
import { profileProviders } from '../profile/profile.providers';
import { tipProviders } from '../tip/tip.providers';
import { RankController } from './rank.controller';
import { RankService } from './rank.service';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [RankController],
  providers: [
    ...profileProviders,
    ...tipProviders,
    RankService,
  ],
  exports: [
    RankService
  ]
})
export class RankModule {}
