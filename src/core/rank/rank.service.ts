import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Profile } from '../profile/entities/profile.entity';
import { TIP_STATUS } from '../tip/entities/status.enum';
import { Tip } from '../tip/entities/tip.entity';

@Injectable()
export class RankService {
    constructor(
        @Inject('TIP_REPOSITORY') private tipRepository: Repository<Tip>,
        @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>,
    ) { }

    //#region CRUD
    async getByProfile(profileId: number) {}
    async getAll() {}
    //#endregion CRUD

    async calcRankPoint(authUserId: number, tip: Tip): Promise<number> {
        const authUser = await this.profileRepository.findOne(authUserId, {relations: ['tip']});
        // sort by endDate DESC
        const tips = authUser.tip.sort((previous, next) => next.endDate.getTime() - previous.endDate.getTime());
        const amount = tip.odds * 100;
        let winSeriesCount = 0;
        for (let i = 0; i < tips.length; i++) {
            const tip = tips[i];
            if(tip.status === TIP_STATUS.WIN){
                winSeriesCount++;
            } else if(tip.status === TIP_STATUS.LOOSE){
                break;
            }
        }
        switch (true) {
            case winSeriesCount == 2:
                return amount * 1.2;
            case winSeriesCount == 3:
                return amount * 1.5;
            case winSeriesCount == 4:
                return amount * 1.75;
            case winSeriesCount == 5:
                return amount * 2;
            case winSeriesCount == 5:
                return amount * 2.5;
            case winSeriesCount > 5:
                return amount * 3;
            default:
                return amount;
        }
    }

    async creditRankPoint(authUserId: number, points: number): Promise<void> {
        const profile = await this.profileRepository.findOne(authUserId);
        if(!profile) throw new NotFoundException(`Profile entity not fount with id ${authUserId}`);
        profile.rankingPoint += points;
        await this.profileRepository.save(profile);
    }
}
 