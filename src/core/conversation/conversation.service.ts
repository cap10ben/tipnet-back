import { BadRequestException, ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Profile } from '../profile/entities/profile.entity';
import { CreateConversationDto } from './dto/create-conversation.dto';
import { UpdateConversationDto } from './dto/update-conversation.dto';
import { UpdateParticipantDTO } from './dto/update-participants.dto';
import { Conversation } from './entities/conversation.entity';
import { IConversation } from './entities/IConversation';

@Injectable()
export class ConversationService {

  constructor(
    @Inject('CONVERSATION_REPOSITORY') private repository: Repository<Conversation>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>
  ) { }

  // TODO create entity mapper
  //#region CRUD
  async create(profileId: number, createConversationDto: CreateConversationDto): Promise<IConversation & Conversation> {
    if (createConversationDto.participant.includes(profileId)) throw new BadRequestException();

    const creator = await this.profileRepository.findOne(profileId);
    if (!creator) throw new NotFoundException();

    const participants = await this.profileRepository.findByIds(createConversationDto.participant);
    if (participants.length === 0) throw new NotFoundException();

    const toCreate: IConversation = {
      createdBy: creator,
      participant: [creator, ...participants],
    }
    return this.repository.save(toCreate);
  }

  async findAll(profileId: number) {
    const authUser = await this.profileRepository.findOne(profileId, { relations: ['conversation'] });
    if (!authUser) throw new NotFoundException();
    const convIds = authUser.conversation.map(c => c.id);
    return this.repository.findByIds(convIds);
  }

  async findOne(profileId: number, id: number) {
    const authUser = await this.profileRepository.findOne(profileId);
    if (!authUser) throw new NotFoundException();
    const conv = await this.repository.findOne(id, { relations: ['participant', 'message'] });
    if (!conv) throw new NotFoundException();
    const isParticipant = conv.participant.some(p => p.id === authUser.id);
    if (!isParticipant) throw new ForbiddenException();
    return conv;
  }

  // update(id: number, updateConversationDto: UpdateConversationDto) {
  //   return `This action updates a #${id} conversation`;
  // }

  remove(id: number) {
    return `This action removes a #${id} conversation`;
  }
  //#endregion CRUD

  async addParticipant(profileId: number, id: number, updateParticipantDTO: UpdateParticipantDTO) {
    const conv = await this.repository.findOne(id, { relations: ['participant', 'createdBy'] });
    if (conv.createdBy.id !== profileId) throw new ForbiddenException();
    const participants = await this.profileRepository.findByIds(updateParticipantDTO.participant);
    conv.participant.push(...participants);
    await this.repository.save(conv);
    return await this.repository.findOne(id, { relations: ['participant', 'createdBy'] });
  }

  //TODO handle delete conv when participant number < 2
  async removeParticipant(profileId: number, id: number, updateParticipantDTO: UpdateParticipantDTO) {
    if (updateParticipantDTO.participant.includes(profileId)) throw new ForbiddenException();
    const conv = await this.repository.findOne(id, { relations: ['participant', 'createdBy'] });
    if (!conv) throw new NotFoundException();
    if (conv.createdBy.id !== profileId) throw new ForbiddenException();
    const toRemove = await this.profileRepository.findByIds(updateParticipantDTO.participant);
    toRemove.forEach(r => {
      const i = conv.participant.findIndex(p => p.id === r.id);
      if (i > -1) conv.participant.splice(i, 1);
    });
    return this.repository.save(conv);
  }
}
