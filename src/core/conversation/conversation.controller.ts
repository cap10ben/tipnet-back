import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, Req, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from '../profile/entities/role.enum';
import { ConversationService } from './conversation.service';
import { CreateConversationDto } from './dto/create-conversation.dto';
import { UpdateConversationDto } from './dto/update-conversation.dto';
import { UpdateParticipantDTO } from './dto/update-participants.dto';

@ApiTags('Conversation')
@Controller('conversation')
@UseInterceptors(AuthUserInterceptor)
export class ConversationController {
  constructor(private readonly conversationService: ConversationService) { }

  //#region CRUD
  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Post()
  create(@Body() createConversationDto: CreateConversationDto, @Req() req) {
   const authUserId: number = req.authInfo.profileId;
    return this.conversationService.create(authUserId, createConversationDto);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Get()
  findAll(@Req() req) {
   const authUserId: number = req.authInfo.profileId;
    return this.conversationService.findAll(authUserId);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Get(':id')
  findOne(@Param('id') id: string, @Req() req) {
   const authUserId: number = req.authInfo.profileId;
    return this.conversationService.findOne(authUserId, +id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateConversationDto: UpdateConversationDto, @Res() res: Response) {
    // return this.conversationService.update(+id, updateConversationDto);
    return res.sendStatus(501);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Res() res: Response) {
    // return this.conversationService.remove(+id);
    return res.sendStatus(501);
  }
  //#endregion CRUD

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Patch(':id/participant/add')
  addParticipants(@Param('id') id: string, @Body() updateParticipantDTO: UpdateParticipantDTO, @Req() req) {
   const authUserId: number = req.authInfo.profileId;
    return this.conversationService.addParticipant(authUserId, +id, updateParticipantDTO);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Patch(':id/participant/remove')
  removeParticipant(@Param('id') id: string, @Body() updateParticipantDTO: UpdateParticipantDTO, @Req() req) {
   const authUserId: number = req.authInfo.profileId;
    return this.conversationService.removeParticipant(authUserId, +id, updateParticipantDTO);
  }
}
