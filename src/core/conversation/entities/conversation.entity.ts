import { Message } from "src/core/message/entities/message.entity";
import { Profile } from "src/core/profile/entities/profile.entity";
import { PrimaryGeneratedColumn, ManyToMany, Entity, OneToMany, ManyToOne, CreateDateColumn, DeleteDateColumn } from "typeorm";

@Entity('conversation')
export class Conversation {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Profile, p => p.id, { nullable: false })
    createdBy: Profile;

    @ManyToMany(() => Profile, p => p.conversation)
    participant: Profile[];

    @OneToMany(() => Message, m => m.conversation)
    message: Message[];

    @CreateDateColumn()
    createdAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
