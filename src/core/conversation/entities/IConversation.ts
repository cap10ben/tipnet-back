import { IProfile } from "src/core/profile/entities/IProfile";

export interface IConversation {

    id?: number;
    createdBy: Partial<IProfile>;
    participant: Partial<IProfile>[];
    // message?: IMessage[];
    createdAt?: Date;
    deletedAt?: Date;
}