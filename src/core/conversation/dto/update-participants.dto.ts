import { ApiProperty } from "@nestjs/swagger";
import { IsArray, ArrayMinSize, IsNumber } from "class-validator";

export class UpdateParticipantDTO {
    
    @ApiProperty()
    @IsArray()
    @ArrayMinSize(1)
    @IsNumber({},{each: true})
    participant: number[];
}