import { ApiProperty } from "@nestjs/swagger";
import { ArrayMinSize, IsArray, IsNumber } from "class-validator";

export class CreateConversationDto {

    @ApiProperty()
    @IsArray()
    @ArrayMinSize(1)
    @IsNumber({},{each: true})
    participant: number[];

}
