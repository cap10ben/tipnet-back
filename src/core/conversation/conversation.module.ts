import { Module } from '@nestjs/common';
import { ConversationService } from './conversation.service';
import { ConversationController } from './conversation.controller';
import { conversationProviders } from './conversation.providers';
import { DatabaseModule } from 'src/database/database.module';
import { JwtModule } from '@nestjs/jwt';
import { profileProviders } from '../profile/profile.providers';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [ConversationController],
  providers: [
    ...conversationProviders,
    ...profileProviders,
    ConversationService,
  ],
})
export class ConversationModule {}
