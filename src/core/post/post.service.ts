import { ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { In, Repository } from 'typeorm';
import { Profile } from '../profile/entities/profile.entity';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { Post } from './entities/post.entity';

@Injectable()
export class PostService {

  constructor(
    @Inject('POST_REPOSITORY') private repository: Repository<Post>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>
  ) { }

  async getAllForFollowing(authUserId: number) {
    const authUser = await this.profileRepository.findOne(authUserId, { relations: ['following'] });
    if(!authUser) throw new ForbiddenException();
    const fIds = authUser.following.map(f => f.id);
    return this.repository.find({ where: { author: { id: In(fIds) } }, relations: ['author', 'comment', 'like'] });
  }

  async create(authUserId: number, createPostDto: CreatePostDto) {
    const author = await this.profileRepository.findOne(authUserId);
    if(!author) throw new ForbiddenException();
    const toCreate = {
      author,
      title: createPostDto.title,
      content: createPostDto.content
    }
    return this.repository.save(toCreate);
  }

  findAll() {
    return `This action returns all post`;
  }

  findOne(id: number) {
    return `This action returns a #${id} post`;
  }

  async update(id: number, authUserId: number, updatePostDto: UpdatePostDto) {
    const author = await this.profileRepository.findOne(authUserId);
    if(!author) throw new ForbiddenException();
    const post = await this.repository.findOne(id);
    if(!post) throw new NotFoundException();
    const toUpdate = {
      ...post,
      title: updatePostDto.title,
      content: updatePostDto.content
    }
    return this.repository.save(toUpdate);
  }

  remove(id: number, authUserId: number) {
    return `This action removes a #${id} post`;
  }
}
