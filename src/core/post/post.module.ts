import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { JwtModule } from '@nestjs/jwt';
import { DatabaseModule } from 'src/database/database.module';
import { postProviders } from './post.providers';
import { profileProviders } from '../profile/profile.providers';
import { CommentModule } from '../comment/comment.module';
import { LikeModule } from '../like/like.module';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
    CommentModule,
    LikeModule
  ],
  controllers: [PostController],
  providers: [
    ...postProviders,
    ...profileProviders,
    PostService,
  ]
})
export class PostModule {}
