import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, Req, Res } from '@nestjs/common';
import { PostService } from './post.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from '../profile/entities/role.enum';
import { Response } from 'express';
import { CommentService } from '../comment/comment.service';
import { CreateCommentDto } from '../comment/dto/create-comment.dto';
import { LikeService } from '../like/like.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Post')
@Controller('post')
@UseInterceptors(AuthUserInterceptor)
export class PostController {
  constructor(
    private readonly postService: PostService,
    private readonly commentService: CommentService,
    private readonly likeService: LikeService,

    ) { }

  // get all profile.following post for /feed
  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Get('feed')
  getAllForFollowing(@Req() req) {
    const authUserId: string = req.authInfo.profileId;
    return this.postService.getAllForFollowing(+authUserId);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Post(':id/comment')
  createCommentForPost(@Param('id') id: string, @Body() createCommentDto: CreateCommentDto, @Req() req) {
    const authUserId: string = req.authInfo.profileId;
    return this.commentService.create(+authUserId, +id, createCommentDto);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Post(':id/like')
  like(@Param('id') id: string, @Req() req) {
    const authUserId: string = req.authInfo.profileId;
    return this.likeService.like(+authUserId, +id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Post(':id/unlike')
  unlike(@Param('id') id: string, @Req() req) {
    const authUserId: string = req.authInfo.profileId;
    return this.likeService.unlike(+authUserId, +id);
  }
  //#region CRUD
  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Post()
  create(@Body() createPostDto: CreatePostDto, @Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.postService.create(+authUserId, createPostDto);
  }

  @Roles(ROLES.ADMIN)
  @Get()
  findAll(@Res() res: Response) {
    return res.sendStatus(501);
    return this.postService.findAll();
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Get(':id')
  findOne(@Param('id') id: string, @Res() res: Response) {
    return res.sendStatus(501);
    return this.postService.findOne(+id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto, @Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.postService.update(+id, +authUserId, updatePostDto);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Delete(':id') // TODO
  remove(@Param('id') id: string, @Req() req, @Res() res: Response) {
    return res.sendStatus(501);
    const authUserId: number = req.authInfo.profileId;
    return this.postService.remove(+id, +authUserId);
  }
  //#endregion CRUD
}
