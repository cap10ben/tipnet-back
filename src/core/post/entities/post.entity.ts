import { Comment } from "src/core/comment/entities/comment.entity";
import { Like } from "src/core/like/entities/like.entity";
import { Profile } from "src/core/profile/entities/profile.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('post')
export class Post {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    content: string;

    @ManyToOne(() => Profile, p => p.id, { nullable: false })
    author: Profile;

    @JoinTable({
        name: 'post_comment'
    })
    @ManyToMany(() => Comment, c => c.id)
    comment: Comment;

    @OneToMany(() => Like, l => l.post, { nullable: false })
    like: Like[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
