import { IProfile } from "src/core/profile/entities/IProfile";

export interface IPost {

    id?: number;
    title: string;
    content: string;
    author: IProfile;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}