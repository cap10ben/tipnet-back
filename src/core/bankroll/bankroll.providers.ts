import { Connection } from "typeorm";
import { Bankroll } from "./entities/bankroll.entity";

export const bankrollProviders = [
    {
        provide: 'BANKROLL_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Bankroll),
        inject: ['DATABASE_CONNECTION']
    }
]