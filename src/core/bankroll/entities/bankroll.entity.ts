import { Profile } from "src/core/profile/entities/profile.entity";
import { Tip } from "src/core/tip/entities/tip.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('bankroll')
export class Bankroll {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({
        default: false
    })
    isActive: boolean;

    @Column({
        default: 0,
        type: 'float',
    })
    amount: number;

    @ManyToOne(() => Profile, p => p.bankroll)
    profile: Profile;

    @OneToMany(() => Tip, t => t.bankroll)
    tip: Tip[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
