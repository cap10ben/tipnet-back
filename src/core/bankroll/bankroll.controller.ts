import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from '../profile/entities/role.enum';
import { BankrollService } from './bankroll.service';
import { CreateBankrollDto } from './dto/create-bankroll.dto';
import { UpdateBankrollDto } from './dto/update-bankroll.dto';

@ApiTags('Bankroll')
@Controller('bankroll')
@UseInterceptors(AuthUserInterceptor)
export class BankrollController {
  constructor(private readonly bankrollService: BankrollService) { }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Patch(':id/active')
  changeActiveBankroll(@Param('id') id: string, @Req() req) {
    const authUserId: number = +req.authInfo.profileId;
    return this.bankrollService.changeActiveBankroll(authUserId, +id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Get(':id/stats/basic')
  getBasicStats(@Param('id') id: string, @Req() req) {
    const authUserId: number = +req.authInfo.profileId;
    return this.bankrollService.getBasicStats(authUserId, +id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Get(':id/stats/basic/detail')
  getBasicStatsDetail(@Param('id') id: string, @Req() req) {
    const authUserId: number = +req.authInfo.profileId;
    return this.bankrollService.getBasicStatsDetail(authUserId, +id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Get(':id/stats/full')
  getFullStats(@Param('id') id: string, @Req() req) {
    const authUserId: number = +req.authInfo.profileId;
    return this.bankrollService.getFullStats(authUserId, +id);
  }

  //#region CRUD
  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Post()
  create(@Body() createBankrollDto: CreateBankrollDto, @Req() req) {
    const authUserId: number = +req.authInfo.profileId;
    return this.bankrollService.create(authUserId, createBankrollDto);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @Get()
  findAll(@Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.bankrollService.findAll(+authUserId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    // return this.bankrollService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBankrollDto: UpdateBankrollDto) {
    // return this.bankrollService.update(+id, updateBankrollDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    // return this.bankrollService.remove(+id);
  }
  //#endregion CRUD
}
