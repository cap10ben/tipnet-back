import { Module } from '@nestjs/common';
import { BankrollService } from './bankroll.service';
import { BankrollController } from './bankroll.controller';
import { bankrollProviders } from './bankroll.providers';
import { DatabaseModule } from 'src/database/database.module';
import { JwtModule } from '@nestjs/jwt';
import { profileProviders } from '../profile/profile.providers';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [BankrollController],
  providers: [
    ...bankrollProviders,
    ...profileProviders,
    BankrollService
  ],
  exports: [
    BankrollService
  ]
})
export class BankrollModule {}
