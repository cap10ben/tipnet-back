import { BadRequestException, ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Profile } from '../profile/entities/profile.entity';
import { ROLES } from '../profile/entities/role.enum';
import { SPORT_CATEGORY } from '../tip/entities/category.enum';
import { TIP_STATUS } from '../tip/entities/status.enum';
import { CreateBankrollDto } from './dto/create-bankroll.dto';
import { UpdateBankrollDto } from './dto/update-bankroll.dto';
import { Bankroll } from './entities/bankroll.entity';

@Injectable()
export class BankrollService {

  constructor(
    @Inject('BANKROLL_REPOSITORY') private repository: Repository<Bankroll>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>,
  ) { }

  async changeActiveBankroll(authUserId: number, bankrollId: number) {
    const authUser = await this.profileRepository.findOne(authUserId, { relations: ['bankroll'] });
    if (!authUser) throw new ForbiddenException();
    //* if authUser is not the owner of the requested bankroll
    if (!authUser.bankroll.map(b => b.id).includes(bankrollId)) throw new ForbiddenException();
    const toActive = await this.repository.findOne(bankrollId);
    if (!toActive) throw new NotFoundException();

    await this.repository.createQueryBuilder()
      .update()
      .set({ isActive: false })
      .where('profileId = :profileId', { profileId: authUserId })
      .execute();

    toActive.isActive = true;
    return this.repository.save(toActive);
  }

  async getBasicStats(authUserId: number, bankrollId: number) {
    const authUser = await this.profileRepository.findOne(authUserId);
    if (!authUser) throw new ForbiddenException();
    const bankroll = await this.repository.findOne(bankrollId, { relations: ['tip'] });
    const basicStats = [TIP_STATUS.WIN, TIP_STATUS.LOOSE, TIP_STATUS.CANCELED].map(status => ({
      status,
      count: bankroll.tip.filter(t => t.status === status).length
    }))
    return basicStats;
  }

  async getBasicStatsDetail(authUserId: number, bankrollId: number) {
    const authUser = await this.profileRepository.findOne(authUserId);
    if (!authUser) throw new ForbiddenException();
    const bankroll = await this.repository.findOne(bankrollId, { relations: ['tip'] });
    const basicStats = [TIP_STATUS.WIN, TIP_STATUS.LOOSE, TIP_STATUS.CANCELED].map(status => ({
      status,
      tips: bankroll.tip.filter(t => t.status === status)
    }))
    return basicStats;
  }

  async getFullStats(authUserId: number, bankrollId: number) {
    const authUser = await this.profileRepository.findOne(authUserId);
    if (!authUser) throw new ForbiddenException();
    const bankroll = await this.repository.findOne(bankrollId, { relations: ['tip'] });
    const tipsForStats = bankroll.tip.filter(t => t.status === TIP_STATUS.WIN || t.status === TIP_STATUS.LOOSE);
    const winTips = tipsForStats.filter(t => t.status === TIP_STATUS.WIN);
    if(winTips.length < 1) throw new BadRequestException('no data');

    const winPercent = winTips.length / tipsForStats.length;
    const oddsAvg = this.getAvg(tipsForStats.map(t => t.odds));
    const amountAvg = this.getAvg(tipsForStats.map(t => t.amount));
    //! dirty but wait for move enum on DB
    const array = Object.values(SPORT_CATEGORY)
    const sports = array.slice(array.length / 2, array.length);
    const sportDistribution = sports.map(status => ({
      status,
      tips: tipsForStats.filter(t => t.category === status)
    }));
    const totalProfitsAmount = this.getSum(winTips.map(t => t.amount * t.odds - t.amount));
    const totalSpendAmout = this.getSum(tipsForStats.map(t => t.amount));
    return {
      winPercent: Math.round(winPercent * 100) / 100,
      oddsAvg: Math.round(oddsAvg * 100) / 100,
      tipsCount: tipsForStats.length,
      amountAvg: Math.round(amountAvg * 100) / 100,
      sportDistribution,
      roi: Math.round((totalProfitsAmount / totalSpendAmout) * 100) / 100,
    }
  }

  private getAvg(array: number[]): number {
    return this.getSum(array) / array.length;
  }

  private getSum(array: number[]): number {
    return array.reduce((previous, current) => previous + current);
  }

  //#region CRUD
  async create(authUserId: number, createBankrollDto: CreateBankrollDto) {
    const authUser = await this.profileRepository.findOne(authUserId);
    if (!authUser) throw new ForbiddenException();
    const toCreate = {
      title: createBankrollDto.title,
      profile: authUser,
    }
    return this.repository.save(toCreate);
  }

  async findAll(authUserId: number) {
    const authUser = await this.profileRepository.findOne(authUserId);
    if (!authUser) throw new ForbiddenException();
    if (authUser.role === ROLES.ADMIN) {
      return this.repository.find({ relations: ['tip'] });
    }
    return this.repository.find({ relations: ['tip'], where: { profile: { id: authUserId } } });
  }

  findOne(authUserId: number, id: number) {
    return `This action returns a #${id} bankroll`;
  }

  update(authUserId: number, id: number, updateBankrollDto: UpdateBankrollDto) {
    return `This action updates a #${id} bankroll`;
  }

  remove(authUserId: number, id: number) {
    return `This action removes a #${id} bankroll`;
  }
  //#endregion CRUD
}
