import { PartialType } from '@nestjs/swagger';
import { CreateBankrollDto } from './create-bankroll.dto';

export class UpdateBankrollDto extends PartialType(CreateBankrollDto) {}
