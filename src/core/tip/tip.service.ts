import { BadRequestException, ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Bankroll } from '../bankroll/entities/bankroll.entity';
import { Profile } from '../profile/entities/profile.entity';
import { ROLES } from '../profile/entities/role.enum';
import { RankService } from '../rank/rank.service';
import { CreateTipDto } from './dto/create-tip.dto';
import { UpdateTipStatusDto } from './dto/update-tip-status.dto';
import { UpdateTipDto } from './dto/update-tip.dto';
import { ITip } from './entities/ITip';
import { TIP_STATUS } from './entities/status.enum';
import { Tip } from './entities/tip.entity';
import { mapToDto } from './tip.mapper';

@Injectable()
export class TipService {

  constructor(
    @Inject('TIP_REPOSITORY') private repository: Repository<Tip>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>,
    @Inject('BANKROLL_REPOSITORY') private bankrollRepository: Repository<Bankroll>,
    private rankService: RankService,
  ) { }

  //#region CRUD
  async create(createTipDto: CreateTipDto, profileId: number): Promise<ITip> {
    const profile = await this.profileRepository.findOne(profileId, { relations: ['bankroll'] }) as Profile;
    if (!profile) throw new ForbiddenException();
    const bankroll = profile.bankroll.find(b => b.isActive);
    const endDate = new Date(createTipDto.endDate);
    if (endDate.valueOf() < Date.now().valueOf()) throw new BadRequestException('endDate must not be greater than now');
    if (createTipDto.status && !TIP_STATUS[createTipDto.status]) throw new BadRequestException('Unknow status');
    const toCreate: Partial<Tip> = {
      isPrivate: createTipDto.isPrivate,
      title: createTipDto.title,
      odds: createTipDto.odds,
      trust: createTipDto.trust,
      description: createTipDto.description,
      category: createTipDto.category,
      type: createTipDto.type,
      bookmaker: createTipDto.bookmaker,
      amount: createTipDto.amount,
      endDate: createTipDto.endDate,
      status: TIP_STATUS.CREATED,
      profile: profile,
      bankroll
    };
    return mapToDto(await this.repository.save(toCreate));
  }

  async findAll(authUserId: number) {
    const authUser = await this.profileRepository.findOne(authUserId);
    if (authUser.role === ROLES.ADMIN) {
      return this.repository.find();
    }
    return this.repository.find({ where: { isPrivate: false } });
  }

  async findOne(id: number, profileId: number, role: ROLES) {
    const tip = await this.repository.findOne(id, { relations: ['profile', 'follower'] });
    if (!tip) throw new NotFoundException();
    if (tip.profile.id === profileId || role === ROLES.ADMIN) {
      return mapToDto(tip);
    }
    if (tip.isPrivate) throw new ForbiddenException(); //? 403 or 404
    return mapToDto(tip);
  }

  async update(id: number, updateTipDto: UpdateTipDto, profileId: number, role: ROLES) {
    const tip = await this.repository.findOne(id, { relations: ['profile'] });
    if (!tip) throw new NotFoundException();
    if (tip.profile.id === profileId || role === ROLES.ADMIN) {
      const tip = await this.repository.findOne(id);
      const toUpdate: UpdateTipDto = {
        ...tip,
        bookmaker: updateTipDto.bookmaker,
        category: updateTipDto.category,
        endDate: updateTipDto.endDate,
        odds: updateTipDto.odds,
        title: updateTipDto.title,
        trust: updateTipDto.trust,
        type: updateTipDto.type,
        description: updateTipDto.description,
        isPrivate: updateTipDto.isPrivate,
        amount: updateTipDto.amount,
      }
      return this.repository.save(toUpdate);
    }
    throw new ForbiddenException()
  }

  async remove(id: number, profileId: number, role: ROLES): Promise<void> {
    const tip = await this.repository.findOne(id, { relations: ['profile'] });
    if (!tip) return;
    if (tip.profile.id === profileId || role === ROLES.ADMIN) {
      await this.repository.delete({ id: tip.id });
    }
    throw new ForbiddenException()
  }
  //#endregion CRUD

  async updateStatus(id: number, updateTipStatusDto: UpdateTipStatusDto, profileId: number) {
    const authUser = await this.profileRepository.findOne(profileId);
    if (!authUser || authUser.role !== ROLES.ADMIN) throw new ForbiddenException();
    const tip = await this.repository.findOne(id, { relations: ['profile', 'bankroll'] });
    if (!tip) throw new NotFoundException();
    const bankroll = tip.bankroll;
    let amount = bankroll.amount;
    if(updateTipStatusDto.status === tip.status) throw new ForbiddenException();
    switch (updateTipStatusDto.status) {
      case TIP_STATUS.CANCELED:
        if (tip.status === TIP_STATUS.WIN || tip.status === TIP_STATUS.LOOSE) throw new BadRequestException('Can not CANCELD if status is WIN OR LOOSE');
        // rembourser bankroll
        amount += tip.amount;
        break;
      case TIP_STATUS.LOOSE:
        if (tip.status !== TIP_STATUS.VALIDATED) throw new BadRequestException('Can not LOOSE if status is not VALIDATED');
        break;
      case TIP_STATUS.VALIDATED:
        if (tip.status !== TIP_STATUS.CREATED) throw new BadRequestException('Can not VALIDATED if status is not CREATED');
        // enlever amount de bankroll.amount
        amount -= tip.amount;
        if (amount < 0) throw new BadRequestException('Bankroll amount is/will be empty');
        break;
      case TIP_STATUS.WIN:
        if (tip.status !== TIP_STATUS.VALIDATED) throw new BadRequestException('Can not WIN if status is not VALIDATED');
        // ajouter amount * odds
        amount += tip.amount * tip.odds;
        const points = await this.rankService.calcRankPoint(tip.profile.id, tip);
        await this.rankService.creditRankPoint(tip.profile.id, points);
        break;
      default:
        throw new BadRequestException();
    }
    const bankrollUpdated = {
      ...bankroll,
      amount
    }
    await this.repository.update({ id: tip.id }, updateTipStatusDto);
    await this.bankrollRepository.save(bankrollUpdated);
  }

  async follow(id: number, profileId: number): Promise<Tip> {
    //* creator can follow his tip to be notified
    const tip = await this.repository.findOne(id, { relations: ['follower'] });
    const follower = await this.profileRepository.findOne({ id: profileId });
    if (!tip) throw new NotFoundException();
    if (tip.follower) {
      tip.follower.push(follower);
    } else {
      tip.follower = [follower];
    }
    return this.repository.save(tip);
  }

  async unfollow(id: number, profileId: number): Promise<Tip> {
    const tip = await this.repository.findOne(id, { relations: ['follower'] });
    const follower = await this.profileRepository.findOne({ id: profileId });
    if (!tip) throw new NotFoundException();
    if (tip.follower) {
      const i = tip.follower.findIndex(p => p.id === follower.id);
      if (i > -1) {
        tip.follower.splice(i, 1);
      } else {
        throw new NotFoundException()
      }
    } else {
      tip.follower = [];
    }
    return this.repository.save(tip);
  }
}
