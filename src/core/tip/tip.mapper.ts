import { mapToLightDto } from "../profile/profile.mapper";
import { ITip } from "./entities/ITip";
import { Tip } from "./entities/tip.entity";

export function mapToDto(tip: Tip): ITip {
    const follower = tip.follower ? tip.follower.map(p => mapToLightDto(p)): [];
    return {
        id: tip.id,
        isPrivate: tip.isPrivate,
        title: tip.title,
        odds: tip.odds,
        trust: tip.trust,
        description: tip.description,
        category: tip.category,
        type: tip.type,
        bookmaker: tip.bookmaker,
        endDate: tip.endDate,
        status: tip.status,
        profile: mapToLightDto(tip.profile),
        follower,
        bankroll: tip.bankroll,
        amount: tip.amount,
    };
}