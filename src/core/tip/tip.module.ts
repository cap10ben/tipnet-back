import { Module } from '@nestjs/common';
import { TipService } from './tip.service';
import { TipController } from './tip.controller';
import { tipProviders } from './tip.providers';
import { DatabaseModule } from 'src/database/database.module';
import { ProfileModule } from '../profile/profile.module';
import { JwtModule } from '@nestjs/jwt';
import { profileProviders } from '../profile/profile.providers';
import { bankrollProviders } from '../bankroll/bankroll.providers';
import { RankModule } from '../rank/rank.module';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
    RankModule
  ],
  controllers: [
    TipController
  ],
  providers: [
    ...tipProviders,
    ...profileProviders,
    ...bankrollProviders,
    TipService,
  ]
})
export class TipModule {}
