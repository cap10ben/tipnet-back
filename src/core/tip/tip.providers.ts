import { Connection } from "typeorm";
import { Tip } from "./entities/tip.entity";

export const tipProviders = [
    {
        provide: 'TIP_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Tip),
        inject: ['DATABASE_CONNECTION']
    }
]