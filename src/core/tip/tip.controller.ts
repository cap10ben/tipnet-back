import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req, UseInterceptors, Res } from '@nestjs/common';
import { TipService } from './tip.service';
import { CreateTipDto } from './dto/create-tip.dto';
import { UpdateTipDto } from './dto/update-tip.dto';
import { UpdateTipStatusDto } from './dto/update-tip-status.dto';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/guard/jwt.guard';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from '../profile/entities/role.enum';
import { Response } from 'express';

@UseGuards(JwtAuthGuard)
@Controller('tip')
@ApiTags('Tip')
export class TipController {
  constructor(private readonly tipService: TipService) { }

  //#region CRUD
  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @UseInterceptors(AuthUserInterceptor)
  @Post()
  create(@Body() createTipDto: CreateTipDto, @Req() req) {
    const profileId: number = +req.authInfo.profileId;
    return this.tipService.create(createTipDto, profileId);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Get()
  findAll(@Req() req) {
    const authUserId: number = +req.authInfo.profileId;
    return this.tipService.findAll(authUserId);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Get(':id')
  findOne(@Param('id') id: string, @Req() req) {
    const { profileId, role } = req.authInfo;
    return this.tipService.findOne(+id, profileId, role);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTipDto: UpdateTipDto, @Req() req) {
    const { profileId, role } = req.authInfo;
    return this.tipService.update(+id, updateTipDto, profileId, role);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER)
  @UseInterceptors(AuthUserInterceptor)
  @Delete(':id')
  async remove(@Param('id') id: string, @Req() req, @Res() res: Response) {
    const { profileId, role } = req.authInfo;
    await this.tipService.remove(+id, profileId, role);
    return res.sendStatus(204);
  }
  //#endregion CRUD

  @Roles(ROLES.ADMIN)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id/status')
  updateStatus(@Param('id') id: string, @Body() updateTipDto: UpdateTipStatusDto, @Req() req) {
    const authUserId = req.authInfo.profileId;
    return this.tipService.updateStatus(+id, updateTipDto, authUserId);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id/follow')
  follow(@Param('id') id: string, @Req() req) {
    const { profileId } = req.authInfo;
    return this.tipService.follow(+id, +profileId);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @UseInterceptors(AuthUserInterceptor)
  @Patch(':id/unfollow')
  unfollow(@Param('id') id: string, @Req() req) {
    const { profileId } = req.authInfo;
    return this.tipService.unfollow(+id, +profileId);
  }

}
