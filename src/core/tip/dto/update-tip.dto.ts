import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString, IsNumber, Max, Min, MaxLength, IsEnum, IsDateString } from 'class-validator';
import { BOOKMARKER } from '../entities/bookmarker.enum';
import { SPORT_CATEGORY } from '../entities/category.enum';
import { TIP_STATUS } from '../entities/status.enum';
import { TIP_TYPE } from '../entities/type.enum';

export class UpdateTipDto {
    @ApiProperty({
        required: false
    })
    @IsBoolean()
    @IsOptional()
    isPrivate?: boolean;

    @ApiProperty()
    @IsString()
    title: string;

    @ApiProperty()
    @IsNumber()
    odds: number;

    @Min(0)
    @ApiProperty()
    @IsNumber()
    amount: number;

    @ApiProperty()
    @IsNumber()
    @Max(1)
    @Min(0)
    trust: number;

    @ApiProperty({
        required: false
    })
    @IsString()
    @MaxLength(1500)
    @IsOptional()
    description?: string;

    @ApiProperty({
        enum: SPORT_CATEGORY,
    })
    @IsEnum(SPORT_CATEGORY)
    category: SPORT_CATEGORY;

    @ApiProperty({
        enum: TIP_TYPE,
    })
    @IsEnum(TIP_TYPE)
    type: TIP_TYPE;

    @ApiProperty({
        enum: BOOKMARKER,
    })
    @IsEnum(BOOKMARKER)
    bookmaker: BOOKMARKER;

    @ApiProperty()
    @IsDateString()
    endDate: Date;
}
