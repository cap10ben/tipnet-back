import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsOptional } from "class-validator";
import { TIP_STATUS } from "../entities/status.enum";
export class UpdateTipStatusDto {

    @ApiProperty({
        enum: TIP_STATUS,
        required: false
    })
    @IsEnum(TIP_STATUS)
    status: TIP_STATUS;
}
