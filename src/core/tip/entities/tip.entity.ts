import { Bankroll } from "src/core/bankroll/entities/bankroll.entity";
import { Profile } from "src/core/profile/entities/profile.entity";
import { Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BOOKMARKER } from "./bookmarker.enum";
import { SPORT_CATEGORY } from "./category.enum";
import { TIP_STATUS } from "./status.enum";
import { TIP_TYPE } from "./type.enum";

@Entity('tip')
export class Tip {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'boolean',
        default: false
    })
    isPrivate: boolean;

    @Column()
    title: string;

    @Column({
        type: 'float'
    })
    odds: number;

    @Column({
        type: 'float',
    })
    trust: number;

    @Column({
        type: 'varchar',
        length: 1500
    })
    description: string;

    @Column({
        type: 'float',
    })
    amount: number;

    @Column({
        type: 'enum',
        enum: SPORT_CATEGORY,
    })
    category: SPORT_CATEGORY;

    @Column({
        type: 'enum',
        enum: TIP_TYPE,
    })
    type: TIP_TYPE;

    @Column({
        type: 'enum',
        enum: BOOKMARKER,
    })
    bookmaker: BOOKMARKER;

    @Column()
    endDate: Date;

    @Column({
        type: 'enum',
        enum: TIP_STATUS,
        default: TIP_STATUS.CREATED
    })
    status: TIP_STATUS;

    @ManyToOne(() => Profile, p => p.id, {nullable: false})
    profile: Profile;

    @ManyToMany(() => Profile, p => p.tipFollowing, {onDelete: 'CASCADE'})
    follower: Profile[];

    @ManyToOne(() => Bankroll, b => b.tip)
    bankroll: Bankroll;
}
