import { Bankroll } from "src/core/bankroll/entities/bankroll.entity";
import { IProfile } from "src/core/profile/entities/IProfile";
import { BOOKMARKER } from "./bookmarker.enum";
import { SPORT_CATEGORY } from "./category.enum";
import { TIP_STATUS } from "./status.enum";
import { TIP_TYPE } from "./type.enum";

export interface ITip {
    
    id?: number;
    isPrivate: boolean;
    title: string;
    odds: number;
    trust: number;
    description: string;
    category: SPORT_CATEGORY;
    type: TIP_TYPE;
    bookmaker: BOOKMARKER;
    endDate: Date;
    status: TIP_STATUS;
    profile?: Partial<IProfile>;
    follower: Partial<IProfile>[];
    bankroll: Partial<Bankroll>;
    amount: number;
}