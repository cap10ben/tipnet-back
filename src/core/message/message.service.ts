import { ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Conversation } from '../conversation/entities/conversation.entity';
import { DiffusionList } from '../diffusion-list/entities/diffusion-list.entity';
import { Profile } from '../profile/entities/profile.entity';
import { CreateMessageDto } from './dto/create-message.dto';
import { Message } from './entities/message.entity';

@Injectable()
export class MessageService {

  constructor(
    @Inject('MESSAGE_REPOSITORY') private repository: Repository<Message>,
    @Inject('CONVERSATION_REPOSITORY') private conversationRepository: Repository<Conversation>,
    @Inject('DIFFUSION_LIST_REPOSITORY') private diffusionListRepository: Repository<DiffusionList>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>
  ) { }

  async createForConversation(profileId: number, conversationId: number, createMessageDto: CreateMessageDto) {
    const author = await this.profileRepository.findOne(profileId);
    if (!author) throw new NotFoundException();
    const conversation = await this.conversationRepository.findOne(conversationId, { relations: ['participant'] });
    if (!conversation) throw new NotFoundException();
    if (!conversation.participant.some(p => p.id === author.id)) throw new ForbiddenException();
    const toCreate = {
      content: createMessageDto.content,
      author,
      conversation,
    }
    await this.repository.save(toCreate);
  }

  async createForDiffusionList(profileId: number, diffusionListId: number, createMessageDto: CreateMessageDto) {
    const author = await this.profileRepository.findOne(profileId);
    if (!author) throw new NotFoundException();
    const diffusionList = await this.diffusionListRepository.findOne(diffusionListId, { relations: ['createdBy']})
    if (!diffusionList) throw new NotFoundException();
    if (diffusionList.createdBy.id !== author.id) throw new ForbiddenException();
    const toCreate = {
      content: createMessageDto.content,
      author,
      diffusionList,
    }
    await this.repository.save(toCreate);
  }
}
