import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, Req, Res } from '@nestjs/common';
import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from '../profile/entities/role.enum';
import { Response } from 'express';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Message')
@Controller('message')
@UseInterceptors(AuthUserInterceptor)
export class MessageController {
  constructor(private readonly messageService: MessageService) { }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Post('conversation/:id')
  createForConversation(@Param('id') id: string, @Body() createMessageDto: CreateMessageDto, @Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.messageService.createForConversation(authUserId, +id, createMessageDto);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Post('diffusion-list/:id')
  createForDiffusionList(@Param('id') id: string, @Body() createMessageDto: CreateMessageDto, @Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.messageService.createForDiffusionList(authUserId, +id, createMessageDto);
  }

  //#region unimplemented
  @Get()
  findAll(@Res() res: Response) {
    return res.sendStatus(501);
  }

  @Get(':id')
  findOne(@Param('id') id: string, @Res() res: Response) {
    return res.sendStatus(501);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMessageDto: UpdateMessageDto, @Res() res: Response) {
    return res.sendStatus(501);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Res() res: Response) {
    return res.sendStatus(501);
  }
  //#endregion unimplemented
}
