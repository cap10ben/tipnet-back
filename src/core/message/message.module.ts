import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { DatabaseModule } from 'src/database/database.module';
import { messageProviders } from './message.providers';
import { JwtModule } from '@nestjs/jwt';
import { diffusionListProviders } from '../diffusion-list/diffusion-list.providers';
import { conversationProviders } from '../conversation/conversation.providers';
import { profileProviders } from '../profile/profile.providers';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [MessageController],
  providers: [
    ...diffusionListProviders,
    ...conversationProviders,
    ...profileProviders,
    ...messageProviders,
    MessageService,
  ],
  exports: []
})
export class MessageModule {}
