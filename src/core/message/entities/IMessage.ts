import { IConversation } from "src/core/conversation/entities/IConversation";
import { IProfile } from "src/core/profile/entities/IProfile";

export interface IMessage {

    id?: number;
    content: string;
    createdAt?: Date;
    deletedAt?: Date;
    author: IProfile;
    conversation: IConversation;
}