import { Conversation } from "src/core/conversation/entities/conversation.entity";
import { DiffusionList } from "src/core/diffusion-list/entities/diffusion-list.entity";
import { Profile } from "src/core/profile/entities/profile.entity";
import { PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, ManyToOne, Entity } from "typeorm";

@Entity('message')
export class Message {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    content: string;

    // map it for sendedAt
    @CreateDateColumn()
    createdAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

    @ManyToOne(() => Profile, p => p.id, {nullable: false})
    author: Profile;

    @ManyToOne(() => Conversation, c => c.id, {
        createForeignKeyConstraints: false
    })
    conversation: Conversation;

    @ManyToOne(() => DiffusionList, d => d.id, {
        createForeignKeyConstraints: false
    })
    diffusionList: DiffusionList;
}
