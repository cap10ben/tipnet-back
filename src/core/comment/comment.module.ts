import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { JwtModule } from '@nestjs/jwt';
import { DatabaseModule } from 'src/database/database.module';
import { commentProviders } from './comment.providers';
import { profileProviders } from '../profile/profile.providers';
import { postProviders } from '../post/post.providers';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [CommentController],
  providers: [
    ...profileProviders,
    ...postProviders,
    ...commentProviders,
    CommentService
  ],
  exports: [CommentService]
})
export class CommentModule {}
