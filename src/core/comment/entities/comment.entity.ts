import { Post } from "src/core/post/entities/post.entity";
import { Profile } from "src/core/profile/entities/profile.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('comment')
export class Comment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    content: string;
    
    @ManyToOne(() => Profile, p => p.id, { nullable: false })
    author: Profile;
    
    @ManyToMany(() => Post, p => p.id, { nullable: false })
    post: Post;
    
    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
