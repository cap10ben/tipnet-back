import { Controller, Get, Post, Body, Patch, Param, Delete, Res, UseInterceptors, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { AuthUserInterceptor } from 'src/auth/auth-user.interceptor';
import { Roles } from 'src/decorator/roles.decorator';
import { ROLES } from '../profile/entities/role.enum';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';

@ApiTags('Comment')
@Controller('comment')
@UseInterceptors(AuthUserInterceptor)
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Post()
  create(@Body() createCommentDto: CreateCommentDto, @Res() res: Response) {
    return res.sendStatus(501);
    // return this.commentService.create(createCommentDto);
  }

  @Roles(ROLES.ADMIN)
  @Get()
  findAll(@Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.commentService.findAll(authUserId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.commentService.findOne(+id);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCommentDto: UpdateCommentDto, @Req() req) {
    const authUserId: number = req.authInfo.profileId;
    return this.commentService.update(+id, authUserId, updateCommentDto);
  }

  @Roles(ROLES.ADMIN, ROLES.TIPSTER, ROLES.FOLLOWER)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.commentService.remove(+id);
  }
}
