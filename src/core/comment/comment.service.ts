import { ForbiddenException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Post } from '../post/entities/post.entity';
import { Profile } from '../profile/entities/profile.entity';
import { ROLES } from '../profile/entities/role.enum';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { Comment } from './entities/comment.entity';

@Injectable()
export class CommentService {

  constructor(
    @Inject('COMMENT_REPOSITORY') private repository: Repository<Comment>,
    @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>,
    @Inject('POST_REPOSITORY') private postRepository: Repository<Post>
  ) { }

  async create(authUserId: number, postId: number, createCommentDto: CreateCommentDto) {
    const author = await this.profileRepository.findOne(authUserId);
    if (!author) throw new ForbiddenException();
    const post = await this.postRepository.findOne(postId);
    const toCreate = {
      author,
      content: createCommentDto.content,
      post,
    }
    return this.repository.save(toCreate);
  }

  async findAll(authUserId: number) {
    const author = await this.profileRepository.findOne(authUserId);
    if (!author) throw new ForbiddenException();
    // if(author.role !== ROLES.ADMIN) throw new ForbiddenException();
    return this.repository.find({relations: ['author']});
  }

  findOne(id: number) {
    return `This action returns a #${id} comment`;
  }

  update(id: number, authUserId: number, updateCommentDto: UpdateCommentDto) {
    return `This action updates a #${id} comment`;
  }

  remove(id: number) {
    return `This action removes a #${id} comment`;
  }
}
