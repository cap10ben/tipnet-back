import { ConflictException, Inject, Injectable } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators'
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { IUser } from './entities/IUser';
import { User } from './entities/user.entity';
import { genSaltSync, hashSync } from 'bcrypt';
@Injectable()
export class UserService {

  constructor(
    @Inject('USER_REPOSITORY') private repository: Repository<User>
  ) { }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const n = await this.repository.count({ email: createUserDto.email });
    if (n > 0) throw new ConflictException();
    const salt = genSaltSync();
    const password = hashSync(createUserDto.password, salt);
    const toCreate: IUser = {
      email: createUserDto.email,
      password,
      salt,
    };
    return this.repository.save(toCreate);
  }

  findAll() {
    return `This action returns all user`;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
