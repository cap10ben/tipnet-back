import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import * as csurf from 'csurf';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  // app.use(csurf());
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
  if (process.env.ENVIRONMENT === "DEVELOPMENT") {
    const config = new DocumentBuilder()
      .setTitle('Tipnet Back')
      .setVersion('1.0')
      .addBearerAuth(
        { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
        'access-token',
      )
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api/swagger-ui.html', app, document);
  }
  await app.listen(process.env.PORT);
}
bootstrap();
