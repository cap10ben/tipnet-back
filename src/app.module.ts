import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './core/user/user.module';
import { ProfileModule } from './core/profile/profile.module';
import { AuthModule } from './auth/auth.module';
import { APP_GUARD } from '@nestjs/core';
import { RoleGuard } from './guard/role.guard';
import { JwtModule } from '@nestjs/jwt';
import { TipModule } from './core/tip/tip.module';
import { MessageModule } from './core/message/message.module';
import { ConversationModule } from './core/conversation/conversation.module';
import { DiffusionListModule } from './core/diffusion-list/diffusion-list.module';
import { PostModule } from './core/post/post.module';
import { CommentModule } from './core/comment/comment.module';
import { LikeModule } from './core/like/like.module';
import { BankrollModule } from './core/bankroll/bankroll.module';
import { CronModule } from './cron/cron.module';
import { RankModule } from './core/rank/rank.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
    AuthModule,
    ProfileModule,
    TipModule,
    UserModule,
    MessageModule,
    ConversationModule,
    DiffusionListModule,
    PostModule,
    CommentModule,
    LikeModule,
    BankrollModule,
    CronModule,
    RankModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: RoleGuard
    }
  ],
})
export class AppModule {}
