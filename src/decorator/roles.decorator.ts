import { SetMetadata } from '@nestjs/common';
import { ROLES } from 'src/core/profile/entities/role.enum';

export const ROLES_KEY = 'roles';
export const Roles = (...roles: ROLES[]) => SetMetadata(ROLES_KEY, roles);