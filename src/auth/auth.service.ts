import { ConflictException, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';

import { compareSync } from 'bcrypt';
import { CreateProfileDto } from 'src/core/profile/dto/create-profile.dto';
import { ProfileService } from 'src/core/profile/profile.service';
import { Profile } from 'src/core/profile/entities/profile.entity';
import { UserService } from 'src/core/user/user.service';
import { User } from 'src/core/user/entities/user.entity';
import { CreateUserDto } from 'src/core/user/dto/create-user.dto';
import { RegisterDTO } from './dto/register.dto';
import { LoginDTO } from './dto/login.dto';

@Injectable()
export class AuthService {
    constructor(
        @Inject('USER_REPOSITORY') private userRepository: Repository<User>,
        @Inject('PROFILE_REPOSITORY') private profileRepository: Repository<Profile>,
        private _userService: UserService,
        private _profileService: ProfileService,
        private _jwtService: JwtService
    ) { }

    //TODO implement token expiration
    async login({email, password}: LoginDTO): Promise<any> {
        const users = await this.userRepository.find({where: { email }});
        if (users.length !== 1) throw new UnauthorizedException()
        const user = users[0];
        const hasAccess = compareSync(password, user.password);
        if (!hasAccess) throw new UnauthorizedException();
        
        const profile = (await this.profileRepository.find({where: {user: {id: user.id}}}))[0];
        const result = {
            email: user.email,
            profileId: profile.id,
            firstname: profile.firstname,
            lastname: profile.lastname,
            role: profile.role,
        };
        return {
            access_token: this._jwtService.sign(result, { secret: process.env.JWT_SECRET })
        };
    }

    async register(registerDTO: RegisterDTO) {
        const userDTO: CreateUserDto = {
            email: registerDTO.email,
            password: registerDTO.password
        };
        const profileDTO: CreateProfileDto = {
            birthdate: registerDTO.birthdate,
            firstname: registerDTO.firstname,
            lastname: registerDTO.lastname,
            phone: registerDTO.phone,
            role: registerDTO.role,
        }
        const user = await this._userService.create(userDTO);
        await this._profileService.create(profileDTO, user);
        return await this.login({email: userDTO.email, password: userDTO.password});
    }

}
