import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsString, MinLength, IsDateString, IsPhoneNumber, IsOptional, IsEnum } from "class-validator";
import { ROLES } from "src/core/profile/entities/role.enum";

export class RegisterDTO {

    @ApiProperty()
    @IsEmail()
    email: string;

    @ApiProperty()
    @IsString()
    @MinLength(6)
    password: string;

    @ApiProperty()
    @IsString()
    @MinLength(2)
    firstname: string;

    @ApiProperty()
    @IsString()
    @MinLength(2)
    lastname: string;

    @ApiProperty()
    @IsDateString()
    birthdate: string;

    @ApiProperty()
    @IsPhoneNumber()
    @IsOptional()
    phone: string;


    @ApiProperty({
        enum: ROLES,
        required: false
    })
    @IsOptional()
    @IsEnum(ROLES)
    role: ROLES
}