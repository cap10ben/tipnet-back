import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { Request } from 'express';

@Injectable()
export class AuthUserInterceptor implements NestInterceptor {
  constructor(
    private _jwtService: JwtService,
  ) { }
  intercept(ctx: ExecutionContext, next: CallHandler): Observable<any> {
    const req = ctx.switchToHttp().getRequest() as Request;
    const token = req.headers.authorization?.split(' ')[1] as string;
    const decoded = this._jwtService.decode(token);
    req.authInfo = decoded;
    return next.handle();
  }
}
