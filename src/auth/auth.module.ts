import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { DatabaseModule } from 'src/database/database.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { ProfileModule } from 'src/core/profile/profile.module';
import { UserModule } from 'src/core/user/user.module';
import { userProviders } from 'src/core/user/user.providers';
import { profileProviders } from 'src/core/profile/profile.providers';

@Module({
  imports: [
    DatabaseModule,
    UserModule,
    ProfileModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1y' },
    }),
  ],
  controllers: [AuthController],
  providers: [
    ...userProviders,
    ...profileProviders,
    AuthService,
    JwtStrategy
  ]
})
export class AuthModule {}
